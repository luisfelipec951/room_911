<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

Use App\Employee;

Use App\Department;

use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $employees = DB::table('employees')
                        ->select(DB::raw('employees.*, department.id as department_id,department.name as department_name'))
                        ->leftJoin('department', 'employees.department_id', '=', 'department.id')
                        ->where('is_delete', null)
                        ->get();
        $departments = Department::all();
        
        return view('dashboard', compact('employees', 'departments'));
    }

    public function filter (Request $request)
    {

        $data = request()->all();

        $validateDates = false;

        if($data['initialDate'] && $data['endDate'])
        {
            $validateDates = array($data['initialDate'], $data['endDate']);
        }
        
        $employees = DB::table('employees')
        ->select(DB::raw('employees.*, department.id as department_id,department.name as department_name'))
        ->leftJoin('department', 'employees.department_id', '=', 'department.id')
        ->leftJoin('access_history', 'employees.card', '=', 'access_history.card')
        ->where('is_delete', null)
        ->when($data['searchByCard'], function ($query, $searchByCard) {
            return $query->where('employees.card', $searchByCard);
        })
        ->when($data['searchByDepartment'], function ($query, $searchByDepartment) {
            return $query->where('department_id', $searchByDepartment);
        })
        ->when($validateDates, function ($query, $validateDates) {
            return $query->whereBetween('access_history.date_access', [$validateDates[0],$validateDates[1]]);
        })
        ->groupBy('employees.id')
        ->get();

        $departments = Department::all();

        return view('dashboard', compact('employees', 'departments'));

    }
}
