<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee;

use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    public function create()
    {
        return view('employee.create');
    }
    public function store(Request $request)
    {
        $data = request()->all();

        Employee::create([
            'card' => $data['card'],
            'first_name' => $data['first_name'],
            'middle_name' => $data['middle_name'],
            'last_name' => $data['last_name'],
            'department_id' => $data['SelectDepartment']
        ]);

        $request->session()->flash('alert-success', 'User was successful created!');

        return redirect()->route('dashboard');
    }

    public function update(Request $request)
    {
        $data = request()->all();

        DB::table('employees')
            ->where('id', $data['id'])
            ->update(['first_name' => $data['first_name'],
                      'middle_name' => $data['middle_name'],
                      'last_name' => $data['last_name'],
                      'department_id' => $data['SelectDepartment']
                      ]);
        
        $request->session()->flash('alert-success', 'User was successful updated!');

        return redirect()->route('dashboard');              
    }

    public function updateState(Request $request)
    {
        $data = request()->all();

        $EmployeeState = DB::table('employees')
        ->where([
            ['id', '=', $data['employee_id']],
        ])->get();

        foreach($EmployeeState as $item)
        {
            if($item->is_enabled == 1)
            {
                $state = 0;
            }
            else
            {
                $state = 1;
            }
        }

        DB::table('employees')
            ->where('id', $data['employee_id'])
            ->update(['is_enabled' => $state]);
        if($state == 1)
        {
            $request->session()->flash('alert-success', 'User was successful enabled!');
        }
        else
        {
            $request->session()->flash('alert-success', 'User was successful disabled!');
        }        
        

        return redirect()->route('dashboard');  
    }

    public function deleteEmployee(Request $request)
    {
        $data = request()->all();

        DB::table('employees')
            ->where('id', $data['employee_id'])
            ->update(['is_delete' => now()]);
        
        $request->session()->flash('alert-success', 'User was successful deleted!');

        return redirect()->route('dashboard'); 
    }
}
