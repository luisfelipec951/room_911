<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\AccessHistory;

use Illuminate\Support\Facades\DB;

class Room_911Controller extends Controller
{
    public function index()
    {
        return view('room_911');
    }

    public function store(Request $request)
    {
        $data = request()->all();

        $isEnabledEmployee = DB::table('employees')
        ->where([
            ['is_enabled', '=', 1],
            ['card', '=', $data['card']],
        ])->get();

        if($isEnabledEmployee->count() > 0)
        {
            $successfull_access = 1;

            DB::table('employees')
            ->where('card', $data['card'])
            ->update(['last_access' => now()]);

            $request->session()->flash('alert-success', 'Welcome to Room 911!');
        }
        else
        {
            $successfull_access = 0;
            $request->session()->flash('alert-danger', 'Employee is disabled o card is incorrect');
        }

        AccessHistory::create([
            'card' => $data['card'],
            'successfull_access' => $successfull_access,
            'date_access' => date('Y-m-d')
        ]);

        return redirect()->route('room_911');
    }

    public function getHistory($card)
    {
        $getHistory = DB::table('access_history')
        ->where([
            ['card', '=', $card],
        ])->get();

        return response()->json(array('success' => true, 'getHistory' => $getHistory));
    }
}
