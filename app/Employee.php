<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = array('card', 'first_name', 'middle_name', 'last_name', 'department_id', 'is_enabled');
}
