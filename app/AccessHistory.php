<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessHistory extends Model
{
    protected $table = 'access_history';
    protected $fillable = array('card', 'successfull_access', 'date_access');
}
