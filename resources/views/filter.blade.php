
    <form method="POST" action="{{ url('filter') }}">
        {!! csrf_field() !!}
        <div class="container">
            <div class="row">
                <div class="col-sm">
                    <div class="form-group">
                        <label for="searchByCard">Search by empl. Id</label>
                        <input type="text" pattern="[0-9]{1,10}" class="form-control" name="searchByCard" id="searchByCard">
                    </div>
                </div>
                <div class="col-sm">
                    <div class="form-group">
                            <label for="searchByDepartment">Department</label>
                            <select name="searchByDepartment" class="form-control" id="searchByDepartment">
                            <option value=''>Please choose one...</option>
                            @foreach($departments as $department)
                                <option value="{{ $department->id }}">{{ $department->name}}</option>
                            @endforeach
                            </select>
                    </div>
                </div>
                <div class="col-sm">
                    <div class="form-group">
                            <label for="initialDate">Initial access date</label>
                            <input type="text" class="form-control" name="initialDate" id="initialDate">
                        </div>
                </div>
                <div class="col-sm">
                        <div class="form-group">
                                <label for="endDate">Final access date</label>
                                <input type="text" class="form-control" name="endDate" id="endDate">
                            </div>
                </div>
                <div class="col-sm">
                    <button type="submit" class="btn btn-primary p-2 mt-4">Search</button>
                </div>
            </div>
        </div>
    </form>
