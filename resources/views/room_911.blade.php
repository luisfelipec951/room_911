@extends('layouts.app')

@section('content')

<div class="col-12 col-md-9 col-xl-8">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <form method="POST" action="{{ url('room_911/create') }}">
        {!! csrf_field() !!}
        <div class="form-group">
            <label for="card">Card</label>
            <input type="text" class="form-control" pattern="[0-9]{1,10}" name="card" id="card" placeholder="Please enter your card">
        </div>
        <button type="submit" class="btn btn-primary">Access Room 911</button>
    </form>
</div>

@endsection