<div id="formNewEmployee" style="display: none;" title="New employee">
        <div style="width: 460px; height: 190px;" id="int_dialog">
          <div style="text-align: justify; font-size: 13px; width: 450px;">
              <form method="POST" action="{{ url('employee/create') }}">
                {!! csrf_field() !!}
                <div class="form-group">
                  <label for="card">Card</label>
                  <input type="text" pattern="[0-9]{1,10}" name="card" class="form-control" id="card" placeholder="Please enter your card" required>
                </div>
                <div class="form-group">
                  <label for="first_name">First name</label>
                  <input type="text" name="first_name" class="form-control" id="first_name" placeholder="Please your first name" required>
                </div>
                <div class="form-group">
                  <label for="middle_name">Middle name</label>
                  <input type="text" name="middle_name" class="form-control" id="middle_name" placeholder="Please your middle name">
                </div>
                <div class="form-group">
                  <label for="last_name">Last name</label>
                  <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Please your last name" required>
                </div>
                <div class="form-group">
                  <label for="SelectDepartment">Department</label>
                  <select name="SelectDepartment" class="form-control" id="SelectDepartment" required>
                    <option value=''>Please choose one...</option>
                    @foreach($departments as $department)
                      <option value="{{ $department->id }}">{{ $department->name}}</option>
                    @endforeach
                  </select>
                </div>
                <button type="submit" class="btn btn-primary btn-sm">Create employee</button>
              </form>
          </div>
        </div>
    </div>