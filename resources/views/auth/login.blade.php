@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
        <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Username</label>
                <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}" placeholder="Enter username">
                    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                    {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                    </div>
                <button class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection