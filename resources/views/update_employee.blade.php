<div id="formUpdateEmployee" style="display: none;" title="Form new employee">
        <div style="width: 460px; height: 190px;" id="int_dialog">
          <div style="text-align: justify; font-size: 13px; width: 450px;">
            <form method="POST" action="{{ url('employee/update') }}">
              {!! csrf_field() !!}
              <input type="hidden" name="id" id="id">
              <div class="form-group">
                <label for="first_name">First name</label>
                <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Please your first name" required>
              </div>
              <div class="form-group">
                <label for="middle_name">Middle name</label>
                <input type="text" class="form-control" name="middle_name" id="middle_name" placeholder="Please your middle name">
              </div>
              <div class="form-group">
                <label for="last_name">Last name</label>
                <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Please your last name" required>
              </div>
              <div class="form-group">
                  <label for="SelectDepartment">Department</label>
                  <select name="SelectDepartment" id="SelectDepartment" class="form-control" required>
                    <option value=''>Please choose one...</option>
                    @foreach($departments as $department)
                      <option value="{{ $department->id }}">{{ $department->name}}</option>
                    @endforeach
                </select>
              </div>
              <button type="submit" class="btn btn-sm btn-primary">Update employee</button>
          </form>
          </div>
        </div>
    </div>