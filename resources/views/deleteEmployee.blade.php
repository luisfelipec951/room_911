
            <form method="POST" action="{{ url('employee/deleteEmployee') }}">
                {!! csrf_field() !!}
              <input type="hidden" id="employee_id" name="employee_id" value="{{ $employee->id }}">
                <button type="submit" class="btn btn-danger btn-sm btn-block mb-2">Delete</button>
            </form>
            