@extends('layouts.app')

@section('content')
<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
</div>
<div class="row">
  <div class="col-sm">
    
  </div>
  <div class="col-sm">
    <p class="text-right">Welcome [{{ auth()->user()->name }}]</p>
  </div>
</div>
<div class="row mb-5">
  <div class="col-sm">
    <form method="POST" action="{{ route('logout') }}">
        {{ csrf_field() }}
        <button class="btn btn-danger float-right btn-sm">Sign off</button>
    </form>
  </div>
</div>
<div class="row mb-5">
    <div class="col-sm text-center">
      <h1>Access Control ROOM_911</h1>
    </div>
  </div>
<div class="row mb-5">
  <div class="col-sm">
    <button class="btn float-right btn-primary btn-sm" onclick="newEmployee()">New employed</button>
  </div>
</div>  
@include('newEmployee')
@include('filter')
<table class="table">
    <thead>
      <tr>
        <th scope="col">Employed ID</th>
        <th scope="col">Department</th>
        <th scope="col">Last name</th>
        <th scope="col">Middle name</th>
        <th scope="col">Firstname</th>
        <th scope="col">Last access</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($employees as $employee)
        <tr>
          <td>{{ $employee->card }}</td>
          <td>{{ $employee->department_name}}</td>
          <td>{{ $employee->last_name }}</td>
          <td>{{ $employee->middle_name }}</td>
          <td>{{ $employee->first_name }}</td>
          <td>{{ $employee->last_access }}</td>
        <td>
          <button id="updateEmployee_{{ $employee->id }}" class="btn btn-primary btn-sm btn-block mb-2"
           data-card="{{ $employee->card }}"
           data-department_name="{{ $employee->department_name }}"
           data-last_name="{{ $employee->last_name }}"
           data-middle_name="{{ $employee->middle_name }}"
           data-first_name="{{ $employee->first_name }}"
           data-department_id="{{ $employee->department_id }}"
           onclick="updateEmployee({{ $employee->id }})">Update</button>
           @include('updateState')
           @include('deleteEmployee')
           <button class="btn btn-primary btn-sm btn-block" onclick="showHistory({{ $employee->card }})">History</button>
           @include('history')
          </td>  
        </tr>
      @endforeach
    </tbody>
  </table>

  @include('update_employee')

@endsection

@section('scripts')

<script>

  function newEmployee()
  {
    $("#formNewEmployee").dialog({ show: "blind",
          hide: "blind",
          width: 500,
          height: 500,
        });
  }

  function updateEmployee(employee_id)
  {
    $("#formUpdateEmployee").dialog({ show: "blind",
          hide: "blind",
          width: 500,
          height: 500,
          open: function( event, ui ) {
            var button = $('#updateEmployee_' + employee_id);
            var data_last_name = button.attr('data-last_name');
            var data_middle_name = button.attr('data-middle_name');
            var data_first_name = button.attr('data-first_name');
            var data_department_id = button.attr('data-department_id');
            $('#formUpdateEmployee input#id').val(employee_id);
            $('#formUpdateEmployee input#first_name').val(data_first_name);
            $('#formUpdateEmployee input#middle_name').val(data_middle_name);
            $('#formUpdateEmployee input#last_name').val(data_last_name);
            $('#formUpdateEmployee select#SelectDepartment').val(data_department_id);
          }
        });
  }

  function showHistory(card)
  {
    $("#history").dialog({ show: "blind",
          hide: "blind",
          width: 500,
          height: 500,
          open: function( event, ui ) {
            
            $.ajax({
              method: 'GET',
              url: '/history/' + card,
              success: function (data) {
                var obj = JSON.parse(JSON.stringify(data));
                var html = "";
                $.each(obj['getHistory'], function (key, val) {
                  if(val.successfull_access == 1)
                  {
                    val.successfull_access = 'Yes'
                  }
                  if(val.successfull_access == 0)
                  {
                    val.successfull_access = 'No'
                  }  
                  html += "<tr><td>" +  val.date_access + "</td><td>" +  val.successfull_access + "</td></tr>"
                });
                $("#historyContent").append(html);
              }
            });
          }
        });
  }

</script>

@endsection