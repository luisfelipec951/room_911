<?php



Route::get('/', 'Auth\LoginController@showLoginForm');

Route::get('dashboard', 'DashboardController@index')->name('dashboard');

Route::post('login', 'Auth\LoginController@login')->name('login');

Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::post('/employee/create', 'EmployeeController@store');

Route::post('/employee/update', 'EmployeeController@update');

Route::get('/room_911', 'Room_911Controller@index')->name('room_911');

Route::post('/room_911/create', 'Room_911Controller@store');

Route::post('/employee/updateState', 'EmployeeController@updateState');

Route::post('/employee/deleteEmployee', 'EmployeeController@deleteEmployee');

Route::get('/history/{card}', 'Room_911Controller@getHistory');

Route::post('filter', 'DashboardController@filter');
