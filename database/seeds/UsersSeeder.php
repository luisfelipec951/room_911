<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'fcastano',
            'email' => 'luisfelipec95@gmail.com',
            'password' => bcrypt('admin'),
            'user_type_id' => 1
        ]);
    }
}
