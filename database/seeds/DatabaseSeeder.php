<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        DB::table('department')->truncate();
        DB::table('user_type')->truncate();
        DB::table('users')->truncate();
        DB::table('employees')->truncate();    
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');

        // $this->call(UsersTableSeeder::class);
        $this->call(DepartmentSeeder::class);
        $this->call(UserTypeSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(EmployeeSeeder::class);
    }
}
