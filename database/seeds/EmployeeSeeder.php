<?php

use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
            'card' => '1144183828',
            'first_name' => 'Luis',
            'middle_name' => 'Felipe',
            'last_name' => 'Castaño',
            'department_id' => 1,
        ]);
    }
}
